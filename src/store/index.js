// import { createStore } from "redux";
import { configureStore } from "@reduxjs/toolkit";
import authReducer from "./auth-slice";
import counterReducer from "./counter-slice";


//one thing to keep in mind we always return one reducer function  basically merger all different slices reducer to one
//configureStore helps with that
const store = configureStore({
  reducer: {counter: counterReducer, auth: authReducer}
});

export default store;

/*
const countReducer = (state = intialState, action) => {
  if (action.type === "INCREMENT") {
    return {
      ...state,
      counter: state.counter + 1,
    };
  }
  if (action.type === "DECREMENT") {
    return {
      ...state,
      counter: state.counter - 1,
    };
  }
  if (action.type === "Increase") {
    return {
      ...state,
      counter: state.counter + action.amount,
    };
  }

  if (action.type === "toggle") {
    return {
      ...state,
      showCounter: !state.showCounter,
    };
  }
  return state;
};
const store = createStore(countReducer);
*/
