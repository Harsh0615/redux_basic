import { createSlice } from "@reduxjs/toolkit";
const initialState = { counter: 0, showCounter: true };
const counterSlice = createSlice({
  name: "count",
  initialState: initialState, //property name here also must be intialState
  reducers: {
    /*Here we can mutate the state directly so to seem(which we shouldn't do by the way). But one good thing 
        about redux toolkit is that it ships with package called immer which look for such kind of state and
        create copy of state object and them merge/override state properties required in that object automatically
        we don't have to worry about that. Here also in reducer functions we get latest state and action */
    increment(state) {
      state.counter++;
    },
    decrement(state) {
      state.counter--;
    },
    increase(state, action) {
      state.counter = state.counter + action.payload;
    },
    toggleCounter(state) {
      state.showCounter = !state.showCounter;
    },
  },
});
export const counterActions = counterSlice.actions;
export default counterSlice.reducer;
